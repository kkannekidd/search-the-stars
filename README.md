# SEARCH THE STARS #

Simple app that demonstrates the interaction between client and server built with Ajax, PHP and JQuery.

* Last modified: November 20th, 2016.
* URL: http://kiddography.com/projects/stars-app/client.html
* Languages: HTML/CSS, Bootstrap, PHP, JavaScript, JQuery, AJAX

## **SOURCES** ##

### HTML/CSS: ###
* [Bootstrap](http://getbootstrap.com/) - 3.3.7
* [Fixed Top Navbar Example for Bootstrap](https://getbootstrap.com/examples/navbar-fixed-top/) - Bootstrap
* [Font Awesome Icons](http://fontawesome.io/icons/) - © Dave Gandy
* [Glyphicons](http://glyphicons.com/) - © Jan Kovarik
* [Default Sizes for Twitter Bootstrap’s Media Queries](https://scotch.io/tutorials/default-sizes-for-twitter-bootstraps-media-queries) - © Mar 04, 2014, Nicholas Cerminara (Scotch.io).
* [Device Media Queries](http://resizr.co/) - © Piers Rueb


### JavaScript/JQuery: ###
* [JQuery](https://jquery.com/) - 2.2.2
* [JQuery Easing Plugin](http://gsgd.co.uk/sandbox/jquery/easing/) - v1.3
* [Collapsible Accordion](http://www.w3schools.com/howto/howto_js_accordion.asp) - w3schools.com - © w3schools.com.
* [Smooth Vertical or Horizontal Page Scrolling with jQuery](http://tympanus.net/codrops/2010/06/02/smooth-vertical-or-horizontal-page-scrolling-with-jquery/) - Codrops - © 06.02.2010, Mary Lou (Codrops).

### Ajax: ###
* [AJAX using JavaScript and JQuery](https://www.udemy.com/learn-ajax-using-javascript-jquery-in-2-hrs-2-projects/learn/v4/t/lecture/4952588?start=105) - © Umang Shah

## **PENDING** ##
* Streamline css id and class names.
* ~~Add footer~~
* ~~Optimize scripts~~
